package ec.solmedia.mplayer.detail

import android.view.View
import ec.solmedia.mplayer.model.MediaItem
import ec.solmedia.mplayer.model.MediaProvider
import ec.solmedia.mplayer.model.Provider

class DetailPresenter(private val view: DetailView, private val provider: Provider = MediaProvider) {

    interface DetailView {
        fun setTitle(title: String)
        fun loadThumb(thumbUrl: String)
        fun setVideoIndicator(visibility: Int)
    }

    fun onCreate(category: String, id: Int) {
        provider.dataAsync(category) { media ->
            media.find { it.id == id }?.let { (_, title, thumbUrl, type) ->
                view.setTitle(title)
                view.loadThumb(thumbUrl)
                view.setVideoIndicator(when (type) {
                    MediaItem.Type.PHOTO -> View.GONE
                    MediaItem.Type.VIDEO -> View.VISIBLE
                })
            }
        }
    }
}