package ec.solmedia.mplayer.detail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import ec.solmedia.mplayer.R
import ec.solmedia.mplayer.commons.load
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity(), DetailPresenter.DetailView {

    private val presenter = DetailPresenter(this)

    companion object {
        val ID = "DetailActivity:id"
        val CATEGORY = "DetailActivity:category"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val id = intent.getIntExtra(ID, -1)
        val category = intent.getStringExtra(CATEGORY)

        presenter.onCreate(category, id)
    }

    override fun setTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun loadThumb(thumbUrl: String) {
        detail_thumb.load(thumbUrl)
    }

    override fun setVideoIndicator(visibility: Int) {
        detail_video_indicator.visibility = visibility
    }
}
