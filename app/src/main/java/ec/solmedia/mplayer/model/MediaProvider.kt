package ec.solmedia.mplayer.model

import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

interface Provider {
    fun dataSync(category: String): List<MediaItem>
    fun dataAsync(category: String, callback: (List<MediaItem>) -> Unit)
}

object MediaProvider : Provider {
    private val thumbBase = "http://lorempixel.com/400/400/"

    /*val data = (1..10).map {
        MediaItem("Title $it", "$thumbBase$it", if (it % 2 == 0) MediaItem.Type.PHOTO else MediaItem.Type.VIDEO)
    }*/

    private var items: List<MediaItem> = emptyList()

    override fun dataSync(category: String): List<MediaItem> {
        Thread.sleep(1000)
        return (1..10).map {
            MediaItem(it, "Title $it", "$thumbBase$category/$it",
                    if (it % 2 == 0) MediaItem.Type.PHOTO else MediaItem.Type.VIDEO, category)
        }
    }

    override fun dataAsync(category: String, callback: (List<MediaItem>) -> Unit) {
        doAsync {
            //if (items.isEmpty()) {
            items = dataSync(category)
            //}

            uiThread {
                callback(items)
            }
        }
    }
}
