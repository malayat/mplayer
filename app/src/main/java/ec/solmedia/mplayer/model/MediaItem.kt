package ec.solmedia.mplayer.model


data class MediaItem(val id: Int, val title: String, val thumbUrl: String, val type: Type, val category: String) {
    enum class Type { PHOTO, VIDEO }
}