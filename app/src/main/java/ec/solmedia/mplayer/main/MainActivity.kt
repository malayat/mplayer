package ec.solmedia.mplayer.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import ec.solmedia.mplayer.R
import ec.solmedia.mplayer.commons.hide
import ec.solmedia.mplayer.commons.show
import ec.solmedia.mplayer.detail.DetailActivity
import ec.solmedia.mplayer.model.Filter
import ec.solmedia.mplayer.model.MediaItem
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity

class MainActivity : AppCompatActivity(), MainPresenter.MainView {

    private val adapter = MediaAdapter { (id, _, _, _, category) ->
        navigateToDetail(id, category)
    }

    private val presenter = MainPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.loadFilterDate()
        rvplayer.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val filter = when (item.itemId) {
            R.id.filter_photos -> Filter.ByType(MediaItem.Type.PHOTO)
            R.id.filter_videos -> Filter.ByType(MediaItem.Type.VIDEO)
            else -> Filter.None
        }
        presenter.loadFilterDate(filter)

        return super.onOptionsItemSelected(item)
    }

    override fun updateData(media: List<MediaItem>) {
        adapter.items = media
    }

    override fun showUI() {
        progressBar.hide()
        rvplayer.show()
    }

    override fun hideUI() {
        progressBar.show()
        rvplayer.hide()
    }

    private fun navigateToDetail(id: Int, category: String) {
        startActivity<DetailActivity>(DetailActivity.ID to id,
                DetailActivity.CATEGORY to category)
    }
}
