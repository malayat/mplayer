package ec.solmedia.mplayer.main

import ec.solmedia.mplayer.model.Filter
import ec.solmedia.mplayer.model.MediaItem
import ec.solmedia.mplayer.model.MediaProvider
import ec.solmedia.mplayer.model.Provider
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class MainPresenter(private val view: MainView, private val provider: Provider = MediaProvider) {

    interface MainView {
        fun updateData(media: List<MediaItem>)
        fun showUI()
        fun hideUI()
    }

    fun loadFilterDate(filter: Filter = Filter.None) {
        view.hideUI()

        async(UI) {
            val technics = bg { provider.dataSync("technics") }
            val cats = bg { provider.dataSync("cats") }
            val nature = bg { provider.dataSync("nature") }

            val items = technics.await() + cats.await() + nature.await()

            val result = when (filter) {
                Filter.None -> items
                is Filter.ByType -> items.filter { it.type == filter.type }
            }

            view.updateData(result)
            view.showUI()
        }
    }
}