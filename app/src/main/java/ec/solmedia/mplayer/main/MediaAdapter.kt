package ec.solmedia.mplayer.main

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import ec.solmedia.mplayer.model.MediaItem
import ec.solmedia.mplayer.R
import ec.solmedia.mplayer.commons.inflate
import ec.solmedia.mplayer.commons.load
import kotlinx.android.synthetic.main.view_media_item.view.*
import kotlin.properties.Delegates

class MediaAdapter(private val itemClick: (MediaItem) -> Unit) :
        RecyclerView.Adapter<MediaAdapter.MediaHolder>() {

    var items: List<MediaItem> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MediaHolder {
        val view = parent.inflate(R.layout.view_media_item)
        return MediaHolder(view)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MediaHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
        holder.itemView.setOnClickListener { itemClick(item) }
    }

    class MediaHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(mediaItem: MediaItem) = with(itemView) {
            media_title.text = mediaItem.title
            media_thumb.load(mediaItem.thumbUrl)
            media_video_indicator.visibility = when (mediaItem.type) {
                MediaItem.Type.VIDEO -> View.VISIBLE
                MediaItem.Type.PHOTO -> View.GONE
            }
        }
    }
}