# Proyecto del curso online de Kotlin

El proyecto es el resultado del curso de Kotlin de Antonio Leiva.

- http://academy.antonioleiva.com

## Anotaciones

### Funciones Ninja

#### when

Reemplazo de switch de java pero mas potente

Cuando se utiliza when como una expresión, hay q comprobar todos los casos

```kotlin
fun test(view: View) {
    val stringType = when(view) {
        is TextView -> "is text view"
        is ViewGroup -> "is view group"
        else -> "unknow"
    }
}

```
Sirve mucho cuando estamos usando _when_ con Enums, ya que si se modifica el Enum agregando un nuevo valor, el compilador fallará indicando que falta una comprobación.

#### with
El parametro pasado por parametro se comporte como si estuvieramos en su contexto

Se puede usar solo o como una expresión y el valor que devuelve es el que indiquemos en la última línea.
Es el ejemplo de abajo, la última linea sería _media_thumb.loadUrl(item.thumbUrl)_ y el _loadUrl_ devuelve _Unit_ entonces la expresión entre corchetes devolverá _Unit_


``` kotlin
with(itemView) {
    media_title.text = item.title
    media_thumb.loadUrl(item.thumbUrl)
}
```

#### apply

Se suele utilizar mucho para inicializar elementos

Se aplica a cualqier cosa como una función de extensión.

```kotlin
val textView = TextView(this).apply {
    text = "apply rules!"
    textSize = 20f
    setOnClickListener {toast("Yeah!")}
}
```

Devuelve el valor así mismo, osea el valor que estamos aplicando _apply_ pero ya inicializado

#### let

Tras una cadena de comprobaciones de nulidad, solo se ejecuta si ninguno de los valores es nulo

```kotlin
fun test(textView: TextView) {
    textView?.text?.let {
        toast(it.toString())
    }
}
```

### Lambdas

Expresion matemática con parámetros de entrada que producen una salida

lambda: (X, Y) -> z

sum: (Int, Int) -> Int

```
{ x,y ->
    val z = x + y
    z               //no se usa la palabra return
}
```

O se puede optimizar

```
{ x, y ->
    x + y    //suma y retorna
}
```

#### Ejemplos

```kotlin
    fun test() {
        val f1: (Int, Int) -> Int = { x, y -> x + y }
        
        //infiriendo tipos
        val sum = { x: Int, y: Int -> x + y }
        applyOp(2, 3, sum)
        
        val mult = { x: Int, y: Int -> x * y }
        applyOp(2, 3, mult)
        
        //se puede especificar la función lambda al llamar a la función
        applyOp(2, 3, {x , y -> x - y})
        
        //si el último parámetro es una lambda se la puede sacar
        applyOp(2, 3) {
            x , y -> x / y
        }
    }
    
    fun applyOp(x: Int, y: Int, f: (Int, Int) -> Int): Int {
        f.invoke(x, y) // se ejecuta la función del parametro con los parámetros
        f(x, y) // se puede ejecutar también sin necesitad de invoke
        
        return f(x, y)
    }
```

#### Ejemplos de listeners con interfaces

En java se tendría que hacer algo así

```kotlin
    interface Callback {
        fun onCallback(result: String)
    }
    
    fun doAsync(x: Int, callback: Callback) {
        //background process
        callback.onCallback("finished")
    }
    
    fun test() {
    
        doAsync(20, object: Callback{
            override fun onCallback(result: String) {
                print(result)
            }
        })
        
    }
```

Lo mismo pero con lambda sería así:

```kotlin
    fun doAsync(x: Int, callback: (String) -> Unit) {
        //background process
        callback("finished")
    }
    
    fun test() {
    
        doAsync(20) { result ->
            print(result)
        }
        
        // o así
        doAsync(20) { print(it) }
        
    }
```

### Delegación de propiedades

__Propiedades en Kotlin__ = variable + getter + setter

#### lazy
Hasta que no se llama por primera vez no se ejecuta el código que lo conlleva

```kotlin
val recyclerView by lazy {
    findViewById(R.id.recyclerView) as RecyclerView
}
```

#### observable
Implementa un observador para manipular los cambios que se generan.

```kotlin
val observedNumer by Delegates.observable(0) { p, old, new ->
    Log.d("observer numer", "old value: $old, new value: $new")
}
```

#### vetoable
Parecido al observable pero ocurre en otro momento, es decir se llama justo antes del cambio de la propiadad y se puede vetar si no cumple con ciertos criterios para permitir o denegar la actualización 
```kotlin
val positiveNumber by Delegates.veotable(0) { p, old, new ->
    new >= 0
}
```
En el ejemplo, la asignación ocurrirá si y solo si el nuevo parámetro es un mayor o igual a cero

#### lateinit
Permite decirle al compilador que no produzca un error por la no inicialización del mismo y que mas tarde se le asignará un valor.

```kotlin
lateinit var recyclerView: RecyclerView
```

### Funciones infix
Permiten usar la nomenclatura infix

```kotlin
fun test() {
    val sum = 10.addition(7)
    
    //al ser una funcion infix se puede realizar lo mismo    
    val sum2 = 10 addition 7
}
    
infix fun Int.addition(other: Int) = this + other
```

### objects

Los objects en kotlin son como los Singletons de java pero bien echos.
Con toda las comprobaciones de multihilo que un singleton necesita en java pero ya listo para usar.

#### Usos
- Como singleton
```kotlin
object MediaProvider {
    val data = "Instancia única"
}
```

- Como constantes
```kotlin
class Test {
    companion object {
        val CONSTANTE = "Constante"
    }
}
```

- Como clase anónima
```kotlin
fun test() {
    recyclerView.setOnClickListener(object : View.OnClickListener {
        override fun onClick(v: View?) {
        
        }  
    })
}
```

## Links

- https://github.com/yole/kotlin-style-guide
